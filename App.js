/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import {
  Alert,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  ScrollView,
  View,
  Text,
} from 'react-native';

const Header = () => (
  <View></View>
)

const RecordingData = (title, pic, duration) => ({
  title,
  pic,
  duration
})
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
const Api = {}
Api.getData = async () => {
  await delay(1000 + (Math.random() * 4000))

  return {
    user: {
      isSubscribed: true,
    },
    spotlight: RecordingData("Gratitude", undefined, 300),
    daily: RecordingData("Daily Meditation", undefined, 322),
    theory: [
      RecordingData("A Conversation with David Whyte",undefined, 354),
      RecordingData("A Conversation with Loch Kelly",undefined, 354),
      RecordingData("Q&A 1 -- Sam Harris",undefined, 354),
      RecordingData("A Conversation with Evan Thompson",undefined, 354),
      RecordingData("A Conversation with Ian McGillacutty",undefined, 354),
      RecordingData("Q&A 2 -- Christopher Hitchens and Richard Dawkins",undefined, 354),
      RecordingData("A Conversation with Leo Babauta",undefined, 354),
      RecordingData("A Conversation with David Whyte",undefined, 354),
      RecordingData("A Conversation with Loch Kelly",undefined, 354),
      RecordingData("Q&A 1 -- Sam Harris",undefined, 354),
      RecordingData("A Conversation with Evan Thompson",undefined, 354),
      RecordingData("A Conversation with Ian McGillacutty",undefined, 354),
      RecordingData("Q&A 2 -- Christopher Hitchens and Richard Dawkins",undefined, 354),
      RecordingData("A Conversation with Leo Babauta",undefined, 354),
      RecordingData("A Conversation with Tim Weaver and Tudge Neuter",undefined, 354),
    ],
    favorites: [
      RecordingData("A Conversation with David Whyte",undefined, 354),
      RecordingData("A Conversation with Loch Kelly",undefined, 354),
      RecordingData("Q&A 1 -- Sam Harris",undefined, 354),
      RecordingData("A Conversation with Evan Thompson",undefined, 354),
      RecordingData("A Conversation with Ian McGillacutty",undefined, 354),
      RecordingData("Q&A 2 -- Christopher Hitchens and Richard Dawkins",undefined, 354),
      RecordingData("A Conversation with Leo Babauta",undefined, 354),
      RecordingData("A Conversation with David Whyte",undefined, 354),
      RecordingData("A Conversation with Loch Kelly",undefined, 354),
      RecordingData("Q&A 1 -- Sam Harris",undefined, 354),
      RecordingData("A Conversation with Evan Thompson",undefined, 354),
      RecordingData("A Conversation with Ian McGillacutty",undefined, 354),
      RecordingData("Q&A 2 -- Christopher Hitchens and Richard Dawkins",undefined, 354),
      RecordingData("A Conversation with Leo Babauta",undefined, 354),
      RecordingData("A Conversation with Tim Weaver and Tudge Neuter",undefined, 354),
    ],
    downloaded: [
      RecordingData("A Conversation with David Whyte",undefined, 354),
      RecordingData("A Conversation with Loch Kelly",undefined, 354),
      RecordingData("Q&A 1 -- Sam Harris",undefined, 354),
      RecordingData("A Conversation with Evan Thompson",undefined, 354),
      RecordingData("A Conversation with Ian McGillacutty",undefined, 354),
      RecordingData("Q&A 2 -- Christopher Hitchens and Richard Dawkins",undefined, 354),
      RecordingData("A Conversation with Leo Babauta",undefined, 354),
      RecordingData("A Conversation with Tim Weaver and Tudge Neuter",undefined, 354),
      RecordingData("A Conversation with David Whyte",undefined, 354),
      RecordingData("A Conversation with Loch Kelly",undefined, 354),
      RecordingData("Q&A 1 -- Sam Harris",undefined, 354),
      RecordingData("A Conversation with Evan Thompson",undefined, 354),
      RecordingData("A Conversation with Ian McGillacutty",undefined, 354),
      RecordingData("Q&A 2 -- Christopher Hitchens and Richard Dawkins",undefined, 354),
      RecordingData("A Conversation with Leo Babauta",undefined, 354),
      RecordingData("A Conversation with Tim Weaver and Tudge Neuter",undefined, 354),
    ],
  }
}

  const FIB_INV = 0.62
  const FIB_REC = 0.38
  const FIB = 1.61803
const RECORDING_WIDTH = 180

const humanizeDuration = seconds => seconds
const Recording = (props) => {
  
  return (
    <View style={styles.recording}>
      <View style={styles.recordingThumbnail}></View>
      <Text style={styles.recordingTitle}>{props.title}</Text>
      <Text style={styles.recordingTitle}>
        {humanizeDuration(props.duration)}
      </Text>
    </View>
  )
}

const ContentList = (
  props = { 
    horizontal: false,
    recordings: [],
  }) => {

 
  return (
    <View style={styles.contentList}>    
      <ScrollView
        horizontal={true}>
        {Array.isArray(props.recordings) 
          ? props.recordings.map(
            recording => (<Recording recording={recording}/>))
          : null
        }
      </ScrollView>      
    </View>
  )
}

const fetchContent = () => {
  return Api.getData()
}

const App = () => {
  const [content, setContent] = useState({
    user: {},
    spotlight: {},
    daily: {},
    theory: [],
    favorites: [],
    downloaded: [],
  })

  useEffect(() => {          

    const fetchIt = async () => {
      const content = await Api.getData()
      setContent(content)
    }
    fetchIt()
  }, [])
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.homeScreen}>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>         
            <Text style={styles.sectionTitle}>{'Featured Content'}</Text>
            <View
              style={styles.lessonSpotlight}>
              {/* <Text style={styles.subscribeButtonText}>{'lesson spotlight'}</Text>
              <Text style={styles.subscribeButtonTextArrow}>{'Gratititude'}</Text> */}
            </View>
            <View
              style={styles.subscribeButton}>
              <Text style={styles.subscribeButtonText}>{'Subscribe now'}</Text>
              <Text style={styles.subscribeButtonTextArrow}>{'❯'}</Text>
            </View>
            <Text style={styles.sectionTitle}>{'practice'}</Text>
            <ContentList
              horizontal={true}
              recordings={[content.daily]}/>
            <Text style={styles.sectionTitle}>{'practice'}</Text>
            <ContentList
              horizontal={true}
              recordings={content.theory}/>
            <Text style={styles.sectionTitle}>{'Favorites'}</Text> 
            <ContentList
              horizontal={true}
              recordings={content.favorites}/>
            <Text style={styles.sectionTitle}>{'Downloaded Content'}</Text>
            <ContentList
              horizontal={true}
              recordings={content.downloaded}/>
          </ScrollView>
        </View>        
      </SafeAreaView>
    </>
  );
};

const COLORS = {
  bg: '#140c1c',
  fg: '#fefefe',
  lighter: '#abef3a',
  white: '#eeeeee',
  black: '#133333',
  dark: '#244444',
  blueVibrant: '#2f67c2',
  yellowVibrant: '#cc8706',
}
const FONT_SIZES = {
  standard: 16,
  jumbo: 38,
}
const STANDARD_MARGIN = FONT_SIZES.standard 
const STANDARD_BORDER_RADIUS = FONT_SIZES.standard * FIB_INV
const CONTENT_SECTION_HEIGHT = 160
const styles = StyleSheet.create({

  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: COLORS.white,
  },
  contentList: {
    marginBottom: STANDARD_MARGIN,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 11,
    fontWeight: '600',
    color: COLORS.fg,
    height: 30,
    textTransform: 'uppercase',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: COLORS.dark,
  },
  highlight: {
    fontWeight: '700',
  },

  homeScreen: {
    // borderColor: 'red',
    // borderWidth: 1,
    backgroundColor: COLORS.bg,
    flexDirection: 'column',
    alignItems: 'center',
  },

  scrollView: {
    // borderWidth: 1,
    // borderColor: 'green',
    width: '84%',
  },

  subscribeButton: {
    backgroundColor: COLORS.blueVibrant,
    height: CONTENT_SECTION_HEIGHT,
    flexDirection: 'row',
    justifyContent: 'center',    
    alignItems: 'center',
    borderRadius: STANDARD_BORDER_RADIUS,
    marginBottom: STANDARD_MARGIN,
  },

  subscribeButtonText: {
    fontSize: FONT_SIZES.jumbo,
    color: COLORS.fg,
    textAlign: 'center',
    marginRight: 20,
    height: FONT_SIZES.jumbo * 1.5,
  },
  subscribeButtonTextArrow: {
    fontSize: FONT_SIZES.jumbo * 1.2,
    color: COLORS.fg,
    textAlign: 'center',
    height: FONT_SIZES.jumbo * 1.7,
  },

  lessonSpotlight: {
    width: '100%',
    aspectRatio: FIB,
    minHeight: 100,
    backgroundColor: COLORS.yellowVibrant,
    marginBottom: STANDARD_MARGIN,
    borderRadius: STANDARD_BORDER_RADIUS,
  },

  footer: {
    color: COLORS.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },

  recording: {
    height: CONTENT_SECTION_HEIGHT,
    width: RECORDING_WIDTH,
    marginRight: 16,
  },
  recordingThumbnail: {
    backgroundColor: 'teal', 
    width: RECORDING_WIDTH, 
    height: RECORDING_WIDTH * FIB_INV,
    borderRadius: STANDARD_BORDER_RADIUS,
  }
});

export default App;
